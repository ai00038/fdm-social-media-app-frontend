app.controller('fdmHomepageController', ['$scope' , 'fdmUsersDataService',function fdmHomepageController($scope,fdmUsersDataService){
	
	/** using $scope to expose public variables and functions for now...  may need to refactor all later. but putting everything in here for this iteration */
	
	// all variables in vm, private first, then exposed
	
	$scope.vm = {
		headerMessage:'Welcome to the FDM Connect!',
		loggedIn:false,
		loginFeedback:null,
		attempted : {
			email : null,
			password : null,
			myQuestion : null,
			myAnswer : null,
			newPassword : null
		},
		securityQuestions : null,
		currentUser : null,
		incorrectLoginAttempts: 0,
		forgotPassword: false,
		correctAnswer: false
	};
	
	
	function login() {
		var key = $scope.vm.attempted.email;
		var pass = $scope.vm.attempted.password;
		var user = null;
		
		if(fdmUsersDataService.allUsers.hasOwnProperty(key)){// if the user exists
			user = fdmUsersDataService.get(key);
			if(pass === user.password){// logs them in for correct password - SUCCESS
				$scope.vm.loggedIn = true;
				$scope.vm.currentUser = user;
				$scope.vm.headerMessage = user.firstName + " " + user.lastName + ", " + user.occupation;
				$scope.vm.loginFeedback = null;// just in case there were previous login attempts that were unsuccessful
			} else {
				$scope.vm.loginFeedback = 'Incorrect password, please try again...';
				//increment login attempts
			}
		} else { // user does not exist :( 
			$scope.vm.loginFeedback = 'User with email: '+ key +' does not exist. Please type a different email address.';
		}
	}
	
	function logout(){
		$scope.vm.headerMessage = 'You are logged out';
		$scope.vm.loggedIn = false;
		$scope.vm.attempted = {
			email : null,
			password : null
		};
		$scope.vm.currentUser = null;
	}
	
	function setForgotPassword(){
		$scope.vm.forgotPassword = true;
	}
	
	function openReset(){
		var key = $scope.vm.attempted.email;
		var user = null;
		
		if(fdmUsersDataService.allUsers.hasOwnProperty(key)){// if the user exists
			user = fdmUsersDataService.get(key);
			$scope.vm.securityQuestions = user.securityQuestions;
		} else { // user does not exist :( 
			$scope.vm.loginFeedback = 'User with email: '+ key +' does not exist. Please type a different email address.';
		}
	}
	
	function answerSecurityQuestion(){
		if($scope.vm.securityQuestions[$scope.vm.attempted.myQuestion].answer===$scope.vm.attempted.myAnswer){// match answer with question
			$scope.vm.correctAnswer = true;
		} else {
			$scope.vm.loginFeedback = 'Incorrect answer to question';//just change feedback
		}
	}
	
	function changePassword(){
		fdmUsersDataService.update($scope.vm.attempted.email,'password',$scope.vm.attempted.newPassword);
		alert("Password now changed!");
		$scope.vm.loginFeedback = null;//change password
		$scope.vm.forgotPassword = false;
	}
	
	// all functions in func, private first, then exposed
	
	$scope.func = {
		login:login,
		logout:logout,
		setForgotPassword : setForgotPassword,
		openReset : openReset,
		answerSecurityQuestion : answerSecurityQuestion,
		changePassword : changePassword,
	};
	
}]);
