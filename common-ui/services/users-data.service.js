app.service('fdmUsersDataService', [function fdmUsersDataService(){

	var _users = {// mock database as a hashmap based on email as the key
		'mary.sue@fdmgroup.com' : {
			email: 'mary.sue@fdmgroup.com',
			password: 'password123',
			firstName: 'Mary',
			lastName: 'Sue',
			occupation: 'Technical Trainee',
			passLoginAttempts : 0,
			securityQuestions : {
				'What was your Mother\'s maiden name?': {
					question:'What was your Mother\'s maiden name?',
					answer:'Williams'
				},
				'Who was your favourite Teacher at School?': {
					question:'Who was your favourite Teacher at School?',
					answer:'Mr Lannister'
				},
				'What was the first car you owned?': {
					question:'What was the first car you owned?',
					answer:'old banger'
				}
			}
		},
		'gary.stu@fdmgroup.com' : {
			email: 'gary.stu@fdmgroup.com',
			password: 'password456',
			firstName: 'Gary',
			lastName: 'Stu',
			occupation: 'Account Manager',
			passLoginAttempts: 0,
			securityQuestions : {
				'What was your Mother\'s maiden name?': {
					question: 'What was your Mother\'s maiden name?',
					answer: 'Richards'
				},
				'Who was your favourite Teacher at School?':{
					question: 'Who was your favourite Teacher at School?',
					answer: 'Miss Tyrell'
				},
				'What was the first car you owned?':{
					question:'What was the first car you owned?',
					answer:'really old banger'
				}
			}
		}
	}; // JSON ASSOCIATIVE ARRAY, MOCK DATA SERVICE FOR USERS, SUBJECT TO REFACTORING
	
	// ALL CRUDS BELOW
	
	function create(){
	
	}
	
	/**
		Acquires user based on their key
	*/
	function get(key){
		return _users[key];
	}
	
	/**
		@param {key, att, val}
		finds user by their {key} (email), alters an {att} of user and changes it to {val}
	*/
	function update(key,att,val){
		_users[key][att] = val;
	}
	
	function remove(){
	
	}
	
	return {
		create : create,
		get : get,
		update : update,
		remove : remove,
		allUsers : _users
	}

}]);